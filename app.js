const formInput = document.querySelector("#taskInput");

const addBtn = document.querySelector('.addButton');

const tasks = document.querySelector(".tasks");

const deleteButton = document.querySelector(".removeBtn");

const filterTask = document.querySelector("#taskFilter");

//All Event listner
updateAllEventListner();

function updateAllEventListner(){
  // Lode DOM Event
  document.addEventListener('DOMContentLoaded', getTasks)
  // Add a task using click
  addBtn.addEventListener('click', addTasks);
  // remove using button
  deleteButton.addEventListener('click', removeAllTask);
  // remove using icon
  tasks.addEventListener('click', removeUsingIcon);
  // filter through the task;
  filterTask.addEventListener('keyup', filterThroughTask);
}


//Event listners
//Get task form Local Storage

function getTasks(){

  
  let allTask;
  if( localStorage.getItem('allTask') === null){
    allTask = [];
  }else{
    allTask = JSON.parse(localStorage.getItem('allTask'))
  }

  allTask.forEach(function(task){
    //create li
    const item = document.createElement('li');
    //add classs name
    item.className = 'items';
    // add teext node
     item.appendChild(document.createTextNode(task));
     //create link element
     const link = document.createElement('a');
     // add class name
     link.className = 'deleteItem'
     // set attribute
     link.setAttribute('href', '#');
     // set icon using font icon
     link.innerHTML = '<li class="fa fa-trash"></i>';
     // append it to li
     item.appendChild(link);
     // append it ul
     tasks.appendChild(item);
  })

}

//Functions Add Task
function addTasks(){
  const item = document.createElement('li');
  item.className = 'items';
   item.appendChild(document.createTextNode(formInput.value));
   const link = document.createElement('a');
   link.className = 'deleteItem'
   link.setAttribute('href', '#');
   link.innerHTML = '<li class="fa fa-trash"></i>';
   item.appendChild(link);
   tasks.appendChild(item);
   // Store task in local Storage
   storeInLocalStorage(formInput.value);
   formInput.value ="";
   
}
//Store in local Storage 

function storeInLocalStorage(task){
  let allTask;
  if( localStorage.getItem('allTask') === null){
    allTask = [];
  }else{
    allTask = JSON.parse(localStorage.getItem('allTask'))
  }

  allTask.push(task);
  localStorage.setItem('allTask', JSON.stringify(allTask))
}

//Function remove All task;

function removeAllTask(e){
 //  tasks.innerHTML = ""
 if( confirm('Are you sure')){
  while(tasks.firstChild){
    tasks.removeChild(tasks.firstChild);
  }
 }
 // Cleartask from Ls
 clearTaskFromLocalStorage(); 
}
// Clear all task from local storage;

function clearTaskFromLocalStorage(){
  localStorage.clear();
}
// remove using Icon;

function removeUsingIcon(e){
  if( e.target.parentElement.classList.contains('deleteItem')){
    if( confirm()){
      e.target.parentElement.parentElement.remove();
      // Remove from Local storage
      removeTaskFromLocalStorage( e.target.parentElement.parentElement);
    }
  
     
  }
}

// Function remove task Item
function removeTaskFromLocalStorage(taskItem){
  let allTask;
  if(localStorage.getItem('allTask') === null){
    allTask = [];
  }else{
    allTask = JSON.parse(localStorage.getItem('allTask'));
  }

  allTask.forEach(function(task, index) {
     if(taskItem.textContent === task){
       allTask.splice(index, 1);
     }
  }) 
  localStorage.setItem('allTask', JSON.stringify(allTask))
  console.log(taskItem);
}


// Filter through task;


function filterThroughTask(e){
  console.log(e.target.value);
  const text = e.target.value.toLowerCase();
   document.querySelectorAll('.items').forEach(function(task){
     const item = task.firstChild.textContent;
     if( item.toLowerCase().indexOf(text) != -1){
       task.style.display = 'block'
     }else{
       task.style.display = 'none';
     }
   })
  
}

